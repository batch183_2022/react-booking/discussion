import {useState, useEffect} from "react";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {UserProvider} from "./UserContext";

import AppNavbar from "./components/AppNavbar";
import Courses from "./pages/Courses";
import CourseView from "./pages/CourseView";
import Error from "./pages/Error";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Register from "./pages/Register";

import {Container} from "react-bootstrap";
import './App.css';

function App() {
// to store the user information and will be used for validating if a user is already logged in on the app or not.
  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })

  // We can check the changes in our User State.
  console.log(user);
  
//Function for clearing local storage on logout.
  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);

    //user act as dependency
  }, [user])

  // To update the User State upon page load if a user already exist.
  useEffect(()=>{
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data =>{
      console.log(data)

      // Set the user states values with the user details upon successful login.
      if(typeof data._id !== "undefined"){

         //This will be set to the user state.
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
        // set back to the initial state of the user.
        // Undefined
        setUser({
          id: null,
          isAdmin: null
        })
      }
     
    })
  }, [])

  return (

    // In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component

    // All other components/pages will be contained in our main component: <App />

    // ReactJS does not like rendering two adjacent elements. instead the adjacent element must be wrapped by a parent element/react fragment

    // ReactJS is a single page application (SPA)
      // Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components

    // We store information in the context by providing the information using the corresponding "Provider" component and passing the information cia the "value" prop/attribute.
    <UserProvider value={{user, setUser, unsetUser}}>
       {/*Router component is used to wrapped around all components which will have access to our routing system.*/}
        <Router>
          <AppNavbar />
          <Container fluid>
              {/*Routes holds all our Route components.*/}
              <Routes>
              {/*
                  - Route assigns an endpoint and displays the appropriate page component for that endpoint.
                  - "path" attribute assigns the endpoint.
                  - "element" attribute assigns page component to be displayed at the endpoint.
              */}
                  <Route exact path ="/" element={<Home />} />
                  <Route exact path ="/courses" element={<Courses />} />
                  <Route exact path ="/courses/:courseId" element={<CourseView />} />
                  <Route exact path ="/register" element={<Register />} />
                  <Route exact path ="/login" element={<Login />} />
                  <Route exact path ="/logout" element={<Logout />} />
                  <Route exact path ="*" element={<Error />} />

              </Routes>
          </Container>
        </Router>
    </UserProvider>
    
  );
}

/*
  Mini Activity

  1. Create a CourseCard component showing a particular course with the name, description and price inside a React-Bootstrap Card:
    - The course name should be in the card title.
    - The description and price should be in the card subtitle.
    - The value for description and price should be in the card text.
    - Add a button for Enroll.
  2. Render the CourseCard component in the Home page.
  3. Take a screenshot of your browser and send it in the batch hangouts

*/

export default App;

