// import coursesData from "../data/coursesData";
import {useEffect, useState} from "react";
import CourseCard from "../components/CourseCard";

export default function Courses(){

	//useState it be used to store th courses retrieved.
	const [courses, setCourses]= useState([]);

	//Retrieve the courses from the database upon initial render of the course component.
	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//To set courses
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course}/>
					)
			}))
		})
	},[])


	// console.log(coursesData);
	// console.log(coursesData[0]);

	// Props
		// is a shorthand for property since components are considered as object in ReactJS.
		// Props is a way to pass data from the parent to child.
		// It is synonymous to the function parameter.

		// Map method
		// const courses = coursesData.map(course =>{
		// 	return(
		// 			<CourseCard key={course.id} courseProp={course} />
		// 		)
		// })
	return(
			<>
				<h1>Courses</h1>
				{courses}
			</>
		)
}